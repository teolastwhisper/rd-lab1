const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");
router.get("/files", function getFiles (req, res) {
  fs.readdir("./files", (err, files) => {
    if (err) {
      res.status(400).json({message:"Client error"});
    }
    else{
      res.status(200).json({message:"Success",files:files});
    }
    
  });
});
const getFileUpdatedDate = (path) => {
  const stats = fs.statSync(path);
  return stats.mtime;
};
router.get("/files/:filename",  function getFile(req, res)  {
  const filename = req.params.filename;
  
    fs.readFile(
      __dirname + "/files/" + filename,
      "utf8",
      (err, data) => {
        if (err) {
          if (err.code === "ENOENT") {
            res.status(400).json({message:`No file with ${filename} filename found`});
          }
        } else {
          const uploadDate = getFileUpdatedDate(
            __dirname + "/files/" + filename
          );
          res.status(200).json({
            message: "Success",
            filename: filename,
            content: data,
            extension: path.extname(filename).substring(1),
            uploadedDate: uploadDate,
          });
        }
      }
    );
 
});
router.post("/files", function createFile (req, res) {
  if (!fs.existsSync("./files")){
    fs.mkdirSync("./files");
}
  const { filename, content } = req.body;
  if (filename && content) {
    fs.writeFile(__dirname + "/files/" + filename, content, (err) => {
      if (err){
      }
      else{
        res.status(200).json({message:"File created successfully"});
      }
    });
   
  } else {
    res.status(400).json({message:"Please specify 'content' parameter"});
  }
});

module.exports = router;
