const express = require('express')
const app = express();
const basicRoutes = require('./basicRoutes.js')
const port = 8080
app.use(express.json())
app.use('/api',basicRoutes)
app.listen(port, () => {
  console.log('Example app listening on port 8080!')
});